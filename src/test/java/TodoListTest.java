import com.sun.tools.javac.comp.Todo;
import org.junit.Test;

import static org.junit.Assert.*;

public class TodoListTest {
    @Test
    public void addTaskReturnFalseOrNull(){
        TodoList todo = new TodoList();
        Task task = null;
        boolean actual = todo.addTask(task);
        assertFalse(actual);
    }

    @Test
    public void addTaskReturnTrueOnObject(){
        TodoList todo = new TodoList();
        Task task = new Task();

        boolean actual = todo.addTask(task);

        assertTrue(actual);
    }

    @Test
    public void getNumberOfTaskReturnZeroOnInitial(){
        TodoList todo = new TodoList();
        int  expected =0;

        int actual = todo.getNumberOfTask();

        assertEquals(expected,actual);
    }

    @Test
    public void getNumberOfTaskReturnOneAfterAddTask(){
        TodoList todo = new TodoList();
    }

}